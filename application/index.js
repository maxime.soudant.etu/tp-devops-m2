'use strict';

const express = require('express');

//Constant
const PORT = 443;
const HOST = '0.0.0.0';
const os = require("os");

//App
const app = express();
let hostname = os.hostname;
app.get('/', (req, res) => {
    res.send(`Hello ! I'm ${hostname} ! `);
});

app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`);
});