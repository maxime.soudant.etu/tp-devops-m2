# Documentation préliminaire

1. **Qu'est-ce que cgroups (Control Groups) dans le contexte des systèmes Linux ?**
    Les cngroups sont un mecanisme de gestion de resources sous linux. Ils sont utilisé poru limiter compter et isoler des ressources entre differents processus. Ils sont essentiel pour garantir une utilisation equitable des resources et prevenir la monopolisation de ces dernière par certains processus. 

2. **Pourquoi les cgroups sont-ils importants dans la conteneurisation ?**
    Les cngroups sont important en conteneurisation car ile permetent le controle et l'isolation de resources ens les containers. Grac à ca, chaque container peut avoir une aprt équitable des resources système, et cela evite une mauvais utilisation des ressources.

3. **Quels sont les principaux sous-systèmes de cgroups et à quoi servent-ils ?**
   Les principaux sous-systèmes de cgroups sont les suivants :
   - **cpu**: Permet de limiter l'utilisation du CPU.
   - **memory**: Limite l'utilisation de la mémoire système.
   - **blkio**: Gère les opérations d'entrée/sortie disque.
   - **net_cls et net_prio**: Gèrent la classification et la priorité du trafic réseau.

4. **Comment pouvez-vous utiliser les cgroups pour limiter la quantité de CPU qu'un conteneur peut utiliser ?**
    On peut :
    - Crer un cgroup spécifisue pour le container
    - créer un fichier de conf pour le cgroup (cgcreate)
    - Définir la limite cpu (cpu.cfs_quota_us)
    - placer le processus du container dans le cgroup (cgexec)
   
5. **Qu'est-ce que le NAT (Network Address Translation) et comment est-il utilisé dans la gestion du réseau des conteneurs ?**
    Le NAT permet de mapper plusieurs ip privé utilisé par des container en une seul ip publique pour la communciation avec internet. Ainsi les container peuvent accéder à internet en utilisant une seule adresse ip publique, celle de l'hote. Cela garantie la sécurité et l'isolation du reseaux de scontainers.
  
6. **Comment pouvez-vous configurer une règle NAT pour permettre à un conteneur d'accéder à Internet depuis un réseau isolé ?**
   Pour cela, il faut :
   - Activer l'IP forwarding sur l'hôte.
   - Créer une interface réseau virtuelle (par exemple, `br0`).
   - Configurer une règle NAT pour rediriger le trafic sortant du conteneur vers l'adresse IP publique de l'hôte.

7. **Ressources en ligne utiles pour comprendre les cgroups et la gestion du réseau des conteneurs :**
   - https://www.kernel.org/doc/Documentation/cgroup-v1/
   - https://docs.docker.com/config/containers/resource_constraints/
   - https://linuxcontainers.org/lxc/introduction/
   - https://docs.docker.com/network/

# Création d'un environnement isolé

1. **Choix du langage**
J'ai choisis d'utiliser simplement bash car nous pouvons directement utiliser les commande de cgroups ce qui est simple. Je voulais au depart partir sur python et créer les cgroups en monipulant directement les dossiers/fichiers mais cela semble compliqué ... De plus, d'après quelques recherche, en C, cela semble etre la même chose, ou des lancement de commandes systèmes... Autant le faire directement en bash ... 

2. **Création d'un groupe de cgroups**
Création d'un cgroups nommé my_container pour les quels on va pouvoir modifier la mémoire et le cpu : 
```sudo cgcreate -g cpu,memory:/my_container```

Definir une limite cpu (20% de un cpu) pour le cgroups my_container :
```sudo cgset -r cpu.max=20000 my_container```

Lancer un script python dans le cgroups my_container. Le processus est donc soumis au limites du cgroup :
```sudo cgexec -g cpu,memory:my_container python my_container_script.py``` 

3. **Création d'un group de cgroups**
Afin de créer un groupe de cgroups : 
```sudo cgcreate -g cpu,memory:/my_container/my_process```

4. **Développement du programme d'isolation**
Voir isolation_V1.sh

5. **Test du programme**
Nous constatons que les limites sont bien prises en compte avec les commande top, htop et cgget. 