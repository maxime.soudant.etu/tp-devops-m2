# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 1.35.0"
    }
  }
}

# Create Ubuntu servers for application
resource "openstack_compute_instance_v2" "Application_vm" {
  name = "Applciation_vm"
  image_name = "ubuntu22.04"
  flavor_name = "puissante"
  key_pair = "MacBook Pro de Maxime"
  security_groups = ["default"]

  network {
    name = "prive"
  }
}

# Create Ubuntu servers for git
resource "openstack_compute_instance_v2" "Git_vm" {
  name = "Git_vm"
  image_name = "ubuntu22.04"
  flavor_name = "puissante"
  key_pair = "MacBook Pro de Maxime"
  security_groups = ["default"]

  network {
    name = "prive"
  }
}

# Create hosts file for ansible
resource "local_file" "inventory" {
  filename = "../ansible/hosts"
  content = <<EOF
[Application_vm]
${openstack_compute_instance_v2.Application_vm.access_ip_v4}
[Git_vm]
${openstack_compute_instance_v2.Git_vm.access_ip_v4}
  EOF
}

