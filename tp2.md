# Création d'un composant de conteneurisation simplifié en autonomie

## Documentation préliminaire

1. Qu'est-ce que cgroups (Control Groups) dans le contexte des systèmes Linux ? 
- Décrivez en quoi consistent les cgroups et pourquoi ils sont utilisés.
2. Pourquoi les cgroups sont-ils importants dans la conteneurisation ? 
- Expliquez comment les cgroups contribuent à l'isolation des ressources entre les conteneurs.
3. Quels sont les principaux sous-systèmes de cgroups et à quoi servent-ils ? 
- Nommez quelques-uns des sous-systèmes de cgroups et expliquez brièvement leurs rôles.
4. Comment pouvez-vous utiliser les cgroups pour limiter la quantité de CPU qu'un conteneur peut utiliser ? 
- Détaillez les étapes pour définir des limites CPU à l'aide des cgroups.
5. Qu'est-ce que le NAT (Network Address Translation) et comment est-il utilisé dans la gestion du réseau des conteneurs ? 
- Expliquez en quoi consiste le NAT et son rôle dans la gestion des communications réseau des conteneurs.
6. Comment pouvez-vous configurer une règle NAT pour permettre à un conteneur d'accéder à Internet depuis un réseau isolé ? 
- Décrivez les étapes de configuration d'une règle NAT pour le réseau de conteneurs.
7. Quelles sont les ressources en ligne, les articles ou les tutoriels que vous avez trouvés utiles pour comprendre les cgroups et la gestion du réseau des conteneurs ?

##  Création d'un environnement isolé
1. Choix du langage de programmation 
- Motivé votre choix
2. Création d'un groupe de cgroups
-  Familiarisez vous avec les commandes cgroups telles que `cgcreate`, `cgset`, `cgexec`, etc.
3. Création d'un groupe de cgroup
- Comment créer un groupe de cgroups pour votre programme en utilisant la commande `cgcreate` ?
- Par exemple, créer un groupe nommé `my_container` pour isoler les ressources du conteneur.
4. Développement du programme d'isolation
-  Développer un programme qui effectue les tâches suivantes :
* Crée un sous-groupe de cgroups pour le processu.
* Configure des limites CPU et mémoire pour ce sous-groupe.
* Exécute un processus (par exemple, un script Python) au sein de ce sous-groupe.
5. Test du programme
- Tester le programme en exécutant un processus gourmand en ressources (par exemple, une boucle infinie) à l'intérieur du sous-groupe.
- Observer que le processus est limité en termes d'utilisation CPU et mémoire.

## Création d'un environnement de conteneur simple
1. Intégration des programmes
- Modifier votre/vos programme(s) pour les intégrer en un seul script ou un ensemble de scripts qui gère à la fois l'isolation des ressources (cgroups) et la gestion du réseau.
- Les programmes devraient être capables de créer un environnement de conteneur simple.
2. Configuration de l'environnement de conteneur
- Configurer votre environnement de conteneur en définissant des limites CPU et mémoire à l'aide des cgroups, en créant une interface réseau virtuelle pour le conteneur et en configurant une règle NAT pour l'accès à Internet.
3. Test de l'environnement de conteneur
-  Tester votre environnement de conteneur en exécutant un processus (par exemple, un serveur web) à l'intérieur du conteneur.
- Vérifiez que le processus du conteneur est correctement isolé en termes de ressources CPU et mémoire, et qu'il peut accéder à Internet.
