#!/bin/bash

# Nom du groupe cgroup
cgroup_name="my_container"

# Créer le groupe cgroup s'il n'existe pas
sudo cgcreate -g cpu,memory:${cgroup_name}

# Limiter l'utilisation CPU à 20% (20000 microsecondes de quota sur une période de 100000)
sudo cgset -r cpu.max=20000 -r ${cgroup_name}

# Limiter la mémoire à 256 Mo
sudo cgset -r memory.max=256M ${cgroup_name}

# Exécuter le processus Python dans le groupe cgroup
sudo cgexec -g cpu,memory:${cgroup_name} python my_script.py
