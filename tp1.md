# TP : Mise en place d'un environnement CI/CD avec Terraform, Ansible et Gitlab CI pour le déploiement automatique d'une application web

## Introduction
Dans le monde de la gestion des infrastructures et du développement logiciel, l'approche DevOps a gagné en importance en tant que pratique visant à améliorer la collaboration entre les équipes de développement et d'exploitation. L'automatisation joue un rôle essentiel dans cette approche, en permettant de déployer rapidement et de manière cohérente des applications sur des infrastructures diverses. Ce TP vise à introduire les étudiants à la mise en place d'un environnement DevOps en utilisant Terraform, Ansible et Gitlab CI pour déployer automatiquement une application web sur des machines virtuelles.

## Objectifs du TP
1. Comprendre les principes de base de DevOps et l'importance de l'automatisation dans le cycle de développement.
2. Apprendre à utiliser Terraform pour créer une infrastructure cloud (machines virtuelles) de manière automatisée.
3. Utiliser Ansible pour configurer les machines virtuelles et les préparer à l'exécution de l'application.
4. Mettre en place un pipeline d'intégration continue (CI) avec Gitlab CI pour automatiser le déploiement de l'application web.

## Instructions

### 1. Mise en place de l'infrastructure avec Terraform
   - Créez un répertoire de projet Terraform.
   - Écrivez les fichiers de configuration Terraform nécessaires pour créer des machines virtuelles.
   - Définissez des variables pour personnaliser la taille, le type et le nombre de machines virtuelles.
   - Documentez votre configuration Terraform.

### 2. Configuration des machines virtuelles avec Ansible
   - Créez un répertoire de projet Ansible.
   - Rédigez des playbooks Ansible pour installer les dépendances requises sur les machines virtuelles.
   - Configurez les machines virtuelles pour accueillir votre application web.
   - Documentez vos playbooks Ansible.

### 3. Développement de l'application web
   - Écrivez une petite application web dans le langage de votre choix.
   - Hébergez le code source de l'application sur un référentiel Gitlab.

### 4. Mise en place du pipeline CI/CD avec Gitlab CI
   - Créez un fichier `.gitlab-ci.yml` dans le référentiel de votre application pour définir les étapes du pipeline.
   - Configurez le pipeline pour déployer automatiquement l'application sur les machines virtuelles créées avec Terraform en utilisant Ansible.
   - Utilisez des variables Gitlab pour stocker les informations sensibles (par exemple, les clés d'accès) de manière sécurisée.
   - Documentez le fichier `.gitlab-ci.yml` et expliquez comment il fonctionne.

### 5. Testez le pipeline
   - Apportez des modifications à votre application web pour déclencher le pipeline CI/CD.
   - Assurez-vous que l'application est déployée automatiquement sur les machines virtuelles.

### 6. Rapport
   - Rédigez un rapport décrivant les étapes que vous avez suivies pour mettre en place l'environnement CI/CD.
   - Incluez des captures d'écran et des explications pour chaque étape.
   - Réfléchissez sur les avantages de l'automatisation dans le contexte DevOps.

