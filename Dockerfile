# Building application
FROM node:16-alpine as builder
WORKDIR /usr/src/app
COPY ./application .
RUN npm ci --only=production
RUN npm install -g @zeit/ncc
RUN ncc build index.js -o dist

FROM node:16-alpine

# Create app directory
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/dist/index.js .
EXPOSE 443
CMD [ "node", "index.js" ]